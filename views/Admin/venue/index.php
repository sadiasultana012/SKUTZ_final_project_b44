<?php
require_once("../../../vendor/autoload.php");

$objVenue = new \App\Admin\venue\Location();

$allData = $objVenue->index();

use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData = $objVenue->search($_REQUEST);
$availableKeywords=$objVenue->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################




######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objVenue->indexPaginator($page,$itemsPerPage);

$serial = (  ($page-1) * $itemsPerPage ) +1;

if($serial<1) $serial=1;

####################### pagination code block#1 of 2 end #########################################



################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objVenue->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################



?>






<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>VENUE - List View</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->


    <style>
        .btn-group .btn {
            transition: background-color .3s ease;
        }

        .panel-table .panel-body {
            padding: 0;
        }

        .table > thead > tr > th {
            border-bottom: none;
        }

        .panel-footer, .panel-table .panel-body .table-bordered {
            border-style: none;
            margin: 0;
        }

        .panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
            text-align: center;
            width: 50px;
        }

        .panel-table .panel-body .table-bordered > thead > tr > th.col-tools {
            text-align: center;
            width: 120px;
        }

        .panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
        .panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
            border-right: 0;
        }

        .panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
        .panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
            border-left: 0;
        }

        .panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td {
            border-bottom: 0;
        }

        .panel-table .panel-body .table-bordered > thead > tr:first-of-type > th {
            border-top: 0;
        }

        .pagination > li > a, .pagination > li > span {
            border-radius: 50% !important;
            margin: 0 5px;
        }

        .pagination {
            margin: 0;
        }


        /* The side navigation menu */
        .sidenav {
            height: 100%; /* 100% Full-height */
            width: 0; /* 0 width - change this with JavaScript */
            position: fixed; /* Stay in place */
            z-index: 1; /* Stay on top */
            top: 0;
            left: 0;
            background-color: #35380f; /* Black*/
            overflow-x: hidden; /* Disable horizontal scroll */
            padding-top: 60px; /* Place content 60px from the top */
            transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
        }

        /* The navigation menu links */
        .sidenav a {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 25px;
            color: #4d5d3b;
            display: block;
            transition: 0.3s
        }

        /* When you mouse over the navigation links, change their color */
        .sidenav a:hover, .offcanvas a:focus{
            color: #f1f1f1;
        }

        /* Position and style the close button (top right corner) */
        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }

        /* Style page content - use this if you want to push the page content to the right when you open the side navigation */
        #main {
            transition: margin-left .5s;
            padding: 20px;
        }

        /* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }




    </style>






</head>
<body>
<div class="container">
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">About</a>
        <a href="#">Services</a>
        <a href="#">Clients</a>
        <a href="#">Contact</a>
    </div>


    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
    <div id="main">
        ...
    </div>

</div>




<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3> <span onclick="openNav()">MENU</span></h3>
                        </div>
                        <div class="col col-xs-6 text-right">
                            <div class="pull-right">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-filter active" data-target="completed">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                        BOOKED
                                    </label>

                                    <a href="trashed.php"  id="option2"   class="btn btn-warning btn-filter role="button">Trash</a> &nbsp;&nbsp;&nbsp;
                                    <label class="btn btn-default btn-filter" data-target="all">
                                        <input type="radio" name="options" id="option3" autocomplete="off"> All
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="mytable" class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th class="col-check"><input type="checkbox" id="checkall" onclick="test()"/></th>
                            <th class="col-tools"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></th>
                            <th style='width: 10%; text-align: center'>Serial Number</th>
                            <th>ID</th>
                            <th class="col-text">Venue Name</th>
                            <th class="col-text">Venue Location</th>
                            <th class="col-text">Venue Capacity</th>
                            <th class="col-text">Venue Cost</th>
                            <th class="col-text">venue Picture</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // $serial= 1;
                        foreach($someData as $oneData){

                            if($serial%2) $bgColor = "AZURE";
                            else $bgColor = "#ffffff";

                            echo "

                  <tr style='background-color: $bgColor'>

                     <td align='center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td align='center'>


                     <a href='edit.php?id=$oneData->id' class='btn btn-default'><span class='glyphicon glyphicon-pencil'
                                                                 aria-hidden='true'></span></a>
                     <a href='trash.php?id=$oneData->id' class='btn btn-danger'><span class='glyphicon glyphicon-trash'
                                                                aria-hidden='true'></span></a>
                      </td>

                     <td align='center'>$serial</td>
                     <td align='center'>$oneData->id</td>

                     <td align='center'>$oneData->venue_name</td>
                      <td align='center'>$oneData->venue_location</td>
                      <td  align='center'>$oneData->venue_capacity</td>
                      <td  align='center'>$oneData->venue_cost</td>


                    <td align='center'><img src='UploadedFiles/$oneData->venue_picture' style=\"width:64px;height:64px;\" />
                    <a href='view.php?id=$oneData->id'>ViewMore</a></td>


                  </tr>
                    ";
                            $serial++;
                        }
                        ?>




                        <tr data-status="completed">
                            <td align="center"><input type="checkbox" class="checkthis"/></td>

                            <td class="hidden-xs">1</td>
                            <td>John Doe</td>
                            <td>johndoe@example.com</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col col-xs-offset-3 col-xs-6">

                            <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                            <div align="left" class="container">
                                <ul class="pagination">

                                    <?php

                                    $pageMinusOne  = $page-1;
                                    $pagePlusOne  = $page+1;


                                    if($page>$pages) Utility::redirect("index.php?Page=$pages");

                                    if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


                                    for($i=1;$i<=$pages;$i++)
                                    {
                                        if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                        else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                                    }
                                    if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                                    ?>

                                    <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange=" javascript:location.href = this.value;" >
                                        <?php
                                        if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                                        else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                                        if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                                        else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                                        if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                                        else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                                        if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6" selected >Show 6 Items Per Page</option>';
                                        else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                                        if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10" selected >Show 10 Items Per Page</option>';
                                        else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                                        if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15" selected >Show 15 Items Per Page</option>';
                                        else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                                        ?>
                                    </select>
                                </ul>
                            </div>
                            <!--  ######################## pagination code block#2 of 2 end ###################################### -->

                        </div>

                        <div class="col col-xs-3">
                            <div class="pull-right">
                                <a href="location.php" class="btn btn-primary role="button"><span class="glyphicon glyphicon-plus"
                                                                                                   aria-hidden="true"></span>Add new </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
        document.body.style.backgroundColor = "white";
    }
    $(document).ready(function () {
        $('.btn-filter').on('click', function () {
            var $target = $(this).data('target');
            if ($target != 'all') {
                $('.table tbody tr').css('display', 'none');
                $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
            } else {
                $('.table tbody tr').css('display', 'none').fadeIn('slow');
            }
        });

        $('#checkall').on('click', function () {
            if ($("#mytable #checkall").is(':checked')) {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });

            } else {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
    });




</script>













<div class="container">

    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>

    <!-- required for search, block 4 of 5 start -->

    <div style="margin-left: 70%">
        <form id="searchForm" action="index.php"  method="get" style="margin-top: 5px; margin-bottom: 10px ">
            <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
            <input type="checkbox"  name="venue_name"   checked  >By Name
            <input type="checkbox"  name="venue_location" checked >By Location
            <input type="checkbox"  name="venue_capacity" checked >By Capacity
            <input type="checkbox"  name="venue_cost" checked >By Cost
            <input hidden type="submit" class="btn-primary" value="search">
        </form>
    </div>

    <!-- required for search, block 4 of 5 end -->


    <form action="trashmultiple.php" method="post" id="multiple">


        <div class="navbar"?>
            <a href="trashed.php"   class="btn btn-info role="button"> View Trashed List</a> &nbsp;&nbsp;&nbsp;
            <a href="create.php "  class="btn btn-primary role="button"> Add new </a>&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" id="delete">Delete  Selected</button>&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-warning">Trash Selected</button>


            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email Active List To A friend</a>

        </div>




        <h1 style="text-align: center" ;">Venue - Active List (<?php echo count($allData) ?>)<h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>

            <th>Select all  <input id="select_all" type="checkbox" value="select all"></th>

            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Venue Name</th>
            <th>Venue Location</th>
            <th>Venue Capacity</th>
            <th>Venue Cost</th>
            <th>venue Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php
       // $serial= 1;
        foreach($someData as $oneData){

            if($serial%2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>

                     <td style='padding-left: 6%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>

                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->venue_name</td>
                      <td>$oneData->venue_location</td>
                      <td>$oneData->venue_capacity</td>
                      <td>$oneData->venue_cost</td>


                    <td style='padding-left: 3%'><img src='UploadedFiles/$oneData->venue_picture' style=\"width:64px;height:64px;\" /></td>

                     <td>
                     <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                     <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                     <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Soft Delete</a>
                     <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Delete</a>
                     <a href='email.php?id=$oneData->id' class='btn btn-success'>Email</a>
                     </td>
                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

    </form>


    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="left" class="container">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;


            if($page>$pages) Utility::redirect("index.php?Page=$pages");

            if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange=" javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6" selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10" selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15" selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->

</div>


<script>
    jQuery(function($) {
        $('#message').fadeIn(500);
        $('#message').fadeOut (500);
        $('#message').fadeIn (500);
        $('#message').delay (2500);
        $('#message').fadeOut (2000);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });



    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });







</script>







<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->





</body>
</html>