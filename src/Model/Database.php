<?php
/**
 * Created by PhpStorm.
 * User: tasni
 * Date: 1/27/2017
 * Time: 11:19 AM
 */

namespace App\Model;
use PDO;
use PDOException;


class Database
{

    public $DBH;
    public function  __construct()
    {
        try{
            $this->DBH = new PDO('mysql:host=localhost;dbname=event_skutz',"root","");
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $error){
            print "Error:".$error->getMessage()."<br>";
            die();


        }

    }

}