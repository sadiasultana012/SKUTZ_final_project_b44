<?php
/**
 * Created by PhpStorm.
 * User: NEXT
 * Date: 2/27/2017
 * Time: 1:07 PM
 */

namespace App\RegisteredUser\venue\VENUE_REGISTER;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;
use PDOException;
class RegLocation extends DB
{
    private $id;
    private $number_of_guest;
    private $email;
    private $contact;
    private $venue_name;
    private $venue_location;



    public function setData($postData)
    {

        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }

        if (array_key_exists('number_of_guest', $postData)) {
            $this->number_of_guest = $postData['number_of_guest'];
        }

        if (array_key_exists('email', $postData)) {
            $this->email = $postData['email'];
        }
        if (array_key_exists('contact', $postData)) {
            $this->contact = $postData['contact'];
        }
        if (array_key_exists('venue_name', $postData)) {
            $this->venue_name = $postData['venue_name'];
        }
        if (array_key_exists('venue_location', $postData)) {
            $this->venue_location = $postData['venue_location'];
        }


    }


    public function REGstore(){

        $arrData = array($this->number_of_guest,$this->email,$this->contact,$this->venue_name,$this->venue_location);

        $sql = "INSERT INTO `venue_registered`( `number_of_guest`, `email`,`contact`,`venue_name`,`venue_location`) VALUES(?,?,?,?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');


    }



}