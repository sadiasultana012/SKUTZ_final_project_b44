<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objVenue = new \App\Admin\venue\Location();
$objVenue->setData($_GET);
$oneData = $objVenue->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Edit Form</title>

    <link rel="stylesheet" type="text/css"  href="../../../views/Admin/venue/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>

    <form class="form-horizontal" action="update.php" method="post" enctype="multipart/form-data">
        <fieldset>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_name">Venue Name </label>
                <div class="col-md-4">
                    <input id="venue_name" name="venue_name" type="text" placeholder="Input your venue name" class="form-control input-md" value="<?php echo $oneData->venue_name ?>" >

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_location"> Venue Location </label>
                <div class="col-md-4">
                    <input id="venue_location" class="form-control input-md" type="text" placeholder="Input your venue location" name="venue_location" value="<?php echo $oneData->venue_location?>">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_capacity">Venue Capacity </label>
                <div class="col-md-4">
                    <input id="venue_capacity" class="form-control input-md" type="number" placeholder="Input your venue_capacity" name="venue_capacity" value="<?php echo $oneData->venue_capacity?>">

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_cost">Venue Cost </label>
                <div class="col-md-4">
                    <input id="venue_cost" class="form-control input-md" type="number" placeholder="Input your venue_cost" name="venue_cost" value="<?php echo $oneData->venue_cost?>">

                </div>
            </div>


            <!-- File Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="filebutton">Venue Picture</label>
                <div class="col-md-4">
                    <input  id="venue_picture"   name="venue_picture" accept=".png, .jpg, .jpeg" class="input-file" type="file">
                    <img src='UploadedFiles/<?php echo $oneData->venue_picture ?>' style="width:100px;height:100px;" />
                </div>
            </div>

            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">



        </fieldset>


        <input type="submit" value="Update">

    </form>


</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


