<?php
/**
 * Created by PhpStorm.
 * User: NEXT
 * Date: 2/23/2017
 * Time: 3:35 PM
 */

namespace App\Admin\venue;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO, PDOException;


class Location extends DB
{
    private $id;
    private $venue_name;
    private $venue_location;
    private $venue_capacity;
    private $venue_cost;
    private $venue_picture;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('venue_name',$postData)) {
            $this->venue_name = $postData['venue_name'];

        }

        if(array_key_exists('venue_location',$postData)) {
            $this->venue_location = $postData['venue_location'];

        }

        if(array_key_exists('venue_capacity',$postData)) {
            $this->venue_capacity = $postData['venue_capacity'];

        }

        if(array_key_exists('venue_cost',$postData)) {
            $this->venue_cost = $postData['venue_cost'];

        }

        if(array_key_exists('venue_picture',$postData)){

            $this->venue_picture = $postData['venue_picture'];

        }



    }



    public function store() {

        $arrData = array($this->venue_name,$this->venue_location,$this->venue_capacity,$this->venue_cost,$this->venue_picture);

        $sql = "INSERT INTO `venue`(`venue_name`, `venue_location`, `venue_capacity`, `venue_cost`,`venue_picture`) VALUES (?,?,?,?,?)";

        $statement = $this->DBH->prepare($sql);

        $result = $statement->execute($arrData);

        if ($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('location.php');


    }

    public function index(){

        $sql = "select * from venue where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();


    }

    public function view(){

        $sql = "select * from venue where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }

    public function trashed(){

        $sql = "select * from venue where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function update(){


        $arrData = array($this->venue_name,$this->venue_location,$this->venue_capacity,$this->venue_cost,$this->venue_picture);
        $sql=  "UPDATE  venue SET venue_name=?,venue_location=?,venue_capacity=?,venue_cost=?,venue_picture=? WHERE id=".$this->id;


        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);


        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :) ");

        Utility::redirect('index.php');


    }

    public function trash(){

        $sql = "UPDATE  venue SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('index.php');


    }

    public function recover(){

        $sql = "UPDATE  venue SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php');


    }

    public function delete(){

        $sql = "Delete from  venue  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from  venue    WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from  venue   WHERE soft_deleted = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from venue  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from venue  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  venue SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }



    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE   venue SET soft_deleted='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }


    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from venue  WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }


    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from venue  WHERE id=".$id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byFileName']) )  $sql = "SELECT * FROM `venue` WHERE `soft_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `venue_picture` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byFileName']) ) $sql = "SELECT * FROM `venue` WHERE `soft_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byFileName']) )  $sql = "SELECT * FROM `venue` WHERE `soft_deleted` ='No' AND `venue_picture` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->venue_name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->venue_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->venue_location);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->venue_location);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end





        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->venue_picture);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->venue_picture);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords














}