-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2017 at 08:07 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `event_skutz`
--
CREATE DATABASE IF NOT EXISTS `event_skutz` DEFAULT CHARACTER SET utf16 COLLATE utf16_unicode_ci;
USE `event_skutz`;

-- --------------------------------------------------------

--
-- Table structure for table `stage_picture`
--

CREATE TABLE IF NOT EXISTS `stage_picture` (
`id` int(11) NOT NULL,
  `stage_name` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `stage_pic` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf16_unicode_ci NOT NULL DEFAULT 'No',
  `about` longtext COLLATE utf16_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `stage_picture`
--

INSERT INTO `stage_picture` (`id`, `stage_name`, `stage_pic`, `soft_deleted`, `about`) VALUES
(1, 'Elegant Blue', 'stage40.jpg', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(2, 'Fairytale Blue', 'Fairytale Blue.jpg', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(3, 'Red Simple', 'stage7.jpg', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(4, 'poiu', 'stage1.jpeg', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.   '),
(5, 'stage2', 'stage2.jpg', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(6, 'Pure in White', 'stage4.JPG', 'No', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(7, 'Lovely Stage', 'stage14.jpg', 'No', 'this one is fine. Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(8, 'Stylish ', 'stage16.jpg', 'No', 'looks goodBeautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(9, 'Royal Red', 'stage24.jpg', 'Yes', 'Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(10, 'Grand', 'stage11.jpg', 'Yes', 'nice one. Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   '),
(11, 'Pretty Stage', 'stage9.jpg', 'Yes', 'This is pretty. Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   Beautiful stage is beautiful, good looking stage is good looking, pretty stage is pretty, modern stage is modern, lovely stage is lovely, smart stage is smart.\r\n   ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes'),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', '5235', 'dfgdg', 'Yes'),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0'),
(20, 'Tasnin', 'Khan', 'Tasnin', '9e95f6d797987b7da0fb293a760fe57e', '01816443356', 'ffwf', '66252a192546294205737b9e936cbc2e'),
(24, 'frag', 'bdsmklszs', 'tasnin18@gmail.com', '6351bf9dce654515bf1ddbd6426dfa97', 'knkfjsilj', 'kjjkfljaksln.', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `venue`
--

CREATE TABLE IF NOT EXISTS `venue` (
`id` int(11) NOT NULL,
  `venue_name` varchar(11) COLLATE utf16_unicode_ci NOT NULL,
  `venue_location` varchar(11) COLLATE utf16_unicode_ci NOT NULL,
  `venue_capacity` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `venue_cost` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `venue_picture` varchar(11) COLLATE utf16_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf16_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `venue`
--

INSERT INTO `venue` (`id`, `venue_name`, `venue_location`, `venue_capacity`, `venue_cost`, `venue_picture`, `soft_deleted`) VALUES
(5, 'k square', 'GEC', '45', '56', 'summm.PNG', 'Yes'),
(6, 'GEC CONVENT', 'GEC', '', '', '', 'Yes'),
(7, 'vhvhmvjhv', 'jgjkglkgk', '5453', '53', '', 'Yes'),
(9, 'hjkhkh', 'hklhkj.bg', '684656', '54545', '16683066_13', 'Yes'),
(10, 'zaza', 'azaz', '4', '23', 'Capture.PNG', 'No'),
(11, 'bhjkggu', 'hbb', '353', '56556', '20161016_18', 'No'),
(12, 'ksquare', 'http://www.', '5455', '5453', 'sum.PNG', 'No'),
(13, 'gec', '', '', '', '', 'No'),
(14, 'jiin', 'hjkhjkhlkbh', '454', '21521', 'summm.PNG', 'No'),
(15, '', '', '', '', '', 'No'),
(16, '', '', '', '', '', 'No'),
(17, 'iijijjildj', 'jjoljjo', '5646', '55', 'summm.PNG', 'No'),
(18, 'jhihhfp', '', '', '', '', 'No'),
(19, 'kjkkl;', 'nklnkln', '54545', '4545', 'Capture.PNG', 'No'),
(20, 'mmknk', ' knk.', '42', '', '', 'Yes'),
(21, 'kmh;b;', 'lmklnkl', '878', '', '', 'No'),
(22, 'kmknnjknf', 'mkfkla', '4546', '535', 'Capture.PNG', 'Yes'),
(23, '', '', '', '', '', 'Yes'),
(24, '', '', '', '', '', 'Yes'),
(25, 'njhjlk', 'njkl.jk', '53454', '656', 'summm.PNG', 'Yes'),
(26, '5yytrutuy', 'gec circle', '485080', '254', 'stage30.jpg', 'No'),
(27, 'ctg 1', 'king of ctg', '50000', '500000', 'stage32.jpg', 'No'),
(28, 'kk', 'ctg', '909', '2000', 'sitting1.jp', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `venue_booking`
--

CREATE TABLE IF NOT EXISTS `venue_booking` (
`id` int(11) NOT NULL,
  `booking_for` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `number_of_guest` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `time` text COLLATE utf16_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `email` text COLLATE utf16_unicode_ci NOT NULL,
  `contact` varchar(11) COLLATE utf16_unicode_ci NOT NULL,
  `venue_name` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `venue_location` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `whenAdded` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf16_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `venue_booking`
--

INSERT INTO `venue_booking` (`id`, `booking_for`, `number_of_guest`, `time`, `date`, `email`, `contact`, `venue_name`, `venue_location`, `whenAdded`, `soft_deleted`) VALUES
(1, '0', '545', '2017-03-01 05:15:09', '2017-03-17', 'tasnin18@gmail.com', '1515', 'njknknl;', '', '0000-00-00 00:00:00', 'No'),
(2, '0', '545', '2017-03-01 00:00:00', '2017-03-17', 'tasnin18@gmail.com', '1515', 'njknknl;', '', '0000-00-00 00:00:00', 'No'),
(3, 'wedding', '545', '2017-03-01 00:00:00', '2017-03-17', 'tasnin18@gmail.com', '1515', 'njknknl;', '', '0000-00-00 00:00:00', 'No'),
(4, 'wedding', '545', '2-5pm', '2017-03-17', 'tasnin18@gmail.com', '1515', 'njknknl;', '', '0000-00-00 00:00:00', 'No'),
(5, 'njhjkhukdh', '55453', '5665', '2017-03-22', 'tasnin18@gmail.com', '42', 'jgjykfjy', '', '0000-00-00 00:00:00', 'No'),
(6, 'gjhgjhkgjk', '242', '2-5pm', '2017-03-16', 'tasnin18@gmail.com', '245245', 'jklkhk', '', '0000-00-00 00:00:00', 'No'),
(7, 'jbjkhbkb', '43554', '54545', '2017-03-03', 'tasnin18@gmail.com', '124', 'vhbjkhlkj', '', '0000-00-00 00:00:00', 'No'),
(8, 'hhkiladfh', '2312245', '1pm-5pm', '2017-03-21', 'tasnin18@gmail.com', '5454544', 'hmhhjh', '', '0000-00-00 00:00:00', 'No'),
(9, 'njhjkhukdhBJHKHK', '55453', '5665', '2017-03-22', 'tasnin18@gmail.com', '42', 'jgjykfjy', '', '0000-00-00 00:00:00', 'No'),
(10, 'gjhgjhkgjk', '242', '2-5pm', '2017-03-16', 'tasnin18@gmail.com', '245245', 'jklkhk', '', '0000-00-00 00:00:00', 'No'),
(11, 'jbjkhbkb', '43554', '54545', '2017-03-03', 'tasnin18@gmail.com', '124', 'vhbjkhlkj', '', '0000-00-00 00:00:00', 'No'),
(12, 'hhkiladfh', '2312245', '1pm-5pm', '2017-03-21', 'tasnin18@gmail.com', '5454544', 'hmhhjh', '', '0000-00-00 00:00:00', 'No'),
(13, 'GKGJ', '44453', '35', '2017-03-15', 'tasnin18@gmail.com', '35345', 'JHJKLHK', '', '0000-00-00 00:00:00', 'No'),
(14, 'birthday,wedding,engagement', '5454', '46546', '2017-03-13', 'tasnin18@gmail.com', '453456456', 'fdsss', 'dhaka', '0000-00-00 00:00:00', 'No'),
(15, 'birthday,wedding', '4554', '4554', '2017-03-15', 'tasnin18@gmail.com', '54542', 'hgjhk', 'chttagong', '0000-00-00 00:00:00', 'No'),
(16, 'birthday,wedding', '455', '454', '2017-03-22', 'tasnin18@gmail.com', '23423', 'ghfhgj', 'chttagong', '0000-00-00 00:00:00', 'No'),
(17, 'birthday,wedding', '455', '454', '2017-03-22', 'tasnin18@gmail.com', '23423', 'ghfhgj', 'chttagong', '03/02/2017 10:34:16 am', 'No'),
(18, 'mehendy_night,corporate', '54545', '4544355', '2017-03-09', 'tasnin18@gmail.com', '545435', ',dlmsls', 'sylhet', '03/02/2017 06:59:26 pm', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `venue_registered`
--

CREATE TABLE IF NOT EXISTS `venue_registered` (
`id` int(111) NOT NULL,
  `number_of_guest` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `contact` varchar(11) COLLATE utf16_unicode_ci NOT NULL,
  `venue_name` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `venue_location` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf16_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `venue_registered`
--

INSERT INTO `venue_registered` (`id`, `number_of_guest`, `email`, `contact`, `venue_name`, `venue_location`, `soft_deleted`) VALUES
(1, '855', 'tasnin18@gmail.com', '5333453', 'mkljld', 'rajshahi', 'No'),
(2, '45435', 'tasnin18@gmail.com', '545', 'jnjk.', 'chttagong', 'No'),
(3, '21454', 'tasnin18@gmail.com', '5454', 'hgykgyg,uk', 'sylhet', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stage_picture`
--
ALTER TABLE `stage_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue`
--
ALTER TABLE `venue`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_booking`
--
ALTER TABLE `venue_booking`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_registered`
--
ALTER TABLE `venue_registered`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stage_picture`
--
ALTER TABLE `stage_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `venue`
--
ALTER TABLE `venue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `venue_booking`
--
ALTER TABLE `venue_booking`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `venue_registered`
--
ALTER TABLE `venue_registered`
MODIFY `id` int(111) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
