<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
$objVenue = new \App\Admin\venue\Location();

if(isset($_POST['mark'])) {

    $objVenue= new \App\Admin\venue\Location();
    $objVenue->recoverMultiple($_POST['mark']);

    Utility::redirect("index.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashed.php");

}