<?php
require_once("../../../vendor/autoload.php");

$objVenue = new \App\Admin\venue\Location();
$objVenue->setData($_GET);
$oneData =$objVenue->view();


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Single Book Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;"> Single Venue Information</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Venue Name</th>
            <th>Venue Location</th>
            <th>Venue Capacity</th>
            <th>Venue Cost</th>
            <th>File Name</th>
            <th>Venue Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->venue_name</td>
                     <td>$oneData->venue_location</td>
                     <td>$oneData->venue_capacity</td>
                     <td>$oneData->venue_cost</td>
                       <td>$oneData->venue_picture</td>
                     <td style='padding-left: 3%'><img src='UploadedFiles/$oneData->venue_picture' style=\"width:64px;height:64px;\" /></td>


                     <td><a href='index.php' class='btn btn-info'>Back To Active List</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>