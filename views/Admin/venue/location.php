<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Event Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css"  href="../../../views/Admin/venue/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {
            font: 400 15px/1.8 Lato, sans-serif;
            color: #777;
        }
        h3, h4 {
            margin: 10px 0 30px 0;
            letter-spacing: 10px;
            font-size: 20px;
            color: #111;
        }
        .container {
            padding: 80px 120px;
        }
        .person {
            border: 10px solid transparent;
            margin-bottom: 25px;
            width: 80%;
            height: 80%;
            opacity: 0.7;
        }
        .person:hover {
            border-color: #f1f1f1;
        }
        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: #fff !important;
        }
        @media (max-width: 600px) {
            .carousel-caption {
                display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
            }
        }
        .bg-1 {
            background: rgba(248, 160, 235, 0.49);;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }
        .btn {
            padding: 10px 20px;
            background-color: #f8a0eb;
            color: #fff;
            border-radius: 10px;
            transition: .2s;
        }
        .btn:hover, .btn:focus {
            border: 1px solid #fff;
            background-color: #f878d6;
            color: #fff;
        }
        .modal-header, h4, .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #ff8621;
        }
        #googleMap {
            width: 100%;
            height: 400px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
            background-color: #f8a0eb;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #f800bd !important;
        }
        .navbar-nav li a:hover {
            color: #fff  !important;
        }
        .navbar-nav li.active a {
            color: #fff !important;
            background-color: rgba(255, 26, 58, 0.75) !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #ff4c2f !important;
        }
        .dropdown-menu li a {
            color: #dbdbde !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
        .form-control {
            border-radius: 0;
        }
        textarea {
            resize: none;
        }








    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">SKRUTZ</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#myPage">WE ARE ENGAGED</a></li>
                <li><a href="#band">WEDDING PLANNER</a></li>
                <li><a href="#tour">dresses</a></li>
                <li><a href="#contact">CONTACT</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">VENUES
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">DHAKA</a></li>
                        <li><a href="#">CHITTAGONG</a></li>
                        <li><a href="#">SYLHET</a></li>
                    </ul>
                </li>
                <li><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>
            </ul>
        </div>
    </div>
</nav>




<div class="container">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="/images/venue/Zhd%20.jpg" alt="New York">
            <div class="carousel-caption">
                <h3>Swiss Park</h3>
                <p></p>
            </div>
        </div>

        <div class="item">
            <img src="/images/venue/swiss.jpg" alt="Chicago">
            <div class="carousel-caption">
                <h3>Zinnurain</h3>
                <p></p>
            </div>
        </div>

        <div class="item">
            <img src="/images/venue/the-peninsula-chittagong.jpg" alt="Los Angeles">
            <div class="carousel-caption">
                <h3>Chittagong Club</h3>
                <p>Even though the traffic was a mess, we had the best time playing at Venice Beach!</p>
            </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
<!-- Container (The View Section) -->
<div class="container">



        <td><a href='index.php' class='btn btn-group-lg button '>List view</a> </td>



    <form class="form-horizontal" action="store.php" method="post" enctype="multipart/form-data">
        <fieldset>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_name">Venue Name </label>
                <div class="col-md-4">
                    <input id="venue_name" name="venue_name" type="text" placeholder="Input your venue name" class="form-control input-md" required>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_location"> Venue Location </label>
                <div class="col-md-4">
                    <input id="venue_location" class="form-control input-md" type="text" placeholder="Input your venue location" name="venue_location" required>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_capacity">Venue Capacity </label>
                <div class="col-md-4">
                    <input id="venue_capacity" class="form-control input-md" type="number" placeholder="Input your venue_capacity" name="venue_capacity" required>

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="venue_capacity">Venue Cost </label>
                <div class="col-md-4">
                    <input id="venue_cost" class="form-control input-md" type="number" placeholder="Input your venue_cost" name="venue_cost" required>

                </div>
            </div>


            <!-- File Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="filebutton">Venue Picture</label>
                <div class="col-md-4">
                    <input  id="venue_picture"  name="venue_picture" accept=".png, .jpg, .jpeg" class="input-file" type="file" required>
                </div>
            </div>





        </fieldset>


        <input type="submit" value="Submit">

    </form>

</div>





<!-- Container (Booking Section) -->
<div id="tour" class="bg-1">
    <div class="container">
        <h3 class="text-center">Hall Booking</h3>
        <p class="text-center"><br> Remember to book your "Venue"!</p>

        <div class="row text-center">
            <div class="col-sm-6">
                <div class="thumbnail">
                    <img src="/images/venue/thumb.jpg" alt="Paris" width="400" height="300">
                    <p><strong>Do u want to book any venue?</strong></p>
                    <p>Start Booking </p>
                    <button class="btn" data-toggle="modal" data-target="#BookingModal">BOOK</button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="thumbnail">
                    <img src="newyork.jpg" alt="New York" width="400" height="300">
                    <p><strong>Do u want to register ur venue?</strong></p>
                    <p>Click the "REGISTER"</p>
                    <button class="btn" data-toggle="modal" data-target="#RegisterModal">REGISTER</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Booking Modal -->
    <div class="modal fade" id="BookingModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4><span class="glyphicon glyphicon-lock"></span> BOOK</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="venueBookingStore.php" method="post">
                        <div class="form-group">
                            <label for="Book"><span class="glyphicon glyphicon-star"></span> Booking For</label><br>
                            <input type="checkbox" name="booking_for[]" value="birthday"  >RECIEPTION<br>
                            <input type="checkbox" name="booking_for[]" value="wedding" > WEDDING<br>
                            <input type="checkbox" name="booking_for[]" value="engagement"  > ENGAGEMENT<br>
                            <input type="checkbox" name="booking_for[]" value="mehendy_night" > MEHENDY NIGHT<br>
                            <input type="checkbox" name="booking_for[]" value="corporate"  > CORPORATE<br>
                            <input type="checkbox" name="booking_for[]" value="birthday"  >BIRTHDAY PARTY<br>
                            <input type="checkbox" name="booking_for[]" value="other" >OTHER<br>


                        </div>
                        <div class="form-group">
                            <label for="psw"><span class="glyphicon glyphicons-group"></span> Nunber Of guest </label>
                            <input type="number" name="number_of_guest"   class="form-control" id="psw" placeholder="How many?" required>
                        </div>

                        <div class="form-group">
                            <label for="Time"><span class="glyphicon glyphicon-time"></span> Enter the time </label>
                            <input type="text" name="time" class="form-control" id="Time" placeholder="2.00pm-5.pm" required>
                        </div>
                        <div class="form-group">
                            <label for="Date"><span class="glyphicon glyphicon-calendar"></span> Enter the Date </label>
                            <input type="date" name="date" class="form-control" id="Date" placeholder="3/5/17" required>
                        </div>
                        <div class="form-group">
                            <label for="usrname"><span class="glyphicon glyphicons-voicemail"></span> Enter your email</label>
                            <input type="email" name="email" class="form-control" id="mail" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label for="contact"><span class="glyphicons glyphicons-ok"></span> Enter your contact </label>
                            <input type="number" name="contact"   class="form-control" id="contact" placeholder="+880" required>
                        </div>

                        <div class="form-group">
                            <label for="Venue Name"><span class="glyphicons glyphicons-ok"></span> Enter Your Venue Choice </label>
                            <input type="text" name="venue_name"   class="form-control" id="venue_name" placeholder="Golden Hotel,Dhaka" required>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectmultiple">Enter Your Venue Location</label>
                            <div class="col-md-4">
                                <select id="selectmultiple" name="venue_location" class="form-control" multiple="multiple">
                                    <option value="dhaka">DHAKA</option>
                                    <option value="chttagong">CHITTAGONG</option>
                                    <option value="sylhet">SYLHET</option>
                                    <option value="rajshahi">RAJSHAHI</option>
                                    <option value="barishal">BARISHAL</option>
                                    <option value="khulna">KHULNA</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-block">SEND
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                    <p>Need <a href="#">help?</a></p>
                </div>
            </div>
        </div>
    </div>

    <!--Register Modal -->
    <div class="modal fade" id="RegisterModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4><span class="glyphicon glyphicon-lock"></span> Register Your VENUE Here</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="venueRegisterStore.php" method="post">

                        <div class="form-group">
                            <label for="psw"><span class="glyphicon glyphicons-group"></span> Capacity Of guest </label>
                            <input type="number" name="number_of_guest"   class="form-control" id="psw" placeholder="How many?" required>
                        </div>

                        <div class="form-group">
                            <label for="usrname"><span class="glyphicon glyphicons-voicemail"></span> Enter your email</label>
                            <input type="email" name="email" class="form-control" id="mail" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label for="contact"><span class="glyphicons glyphicons-ok"></span> Enter your contact </label>
                            <input type="number" name="contact"   class="form-control" id="contact" placeholder="+880" required>
                        </div>

                        <div class="form-group">
                            <label for="Venue Name"><span class="glyphicons glyphicons-ok"></span> Enter Your Venue Name</label>
                            <input type="text" name="venue_name"   class="form-control" id="venue_name" placeholder="Golden Hotel,Dhaka" required>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectmultiple">Enter Your Venue Location</label>
                            <div class="col-md-4">
                                <select id="selectmultiple" name="venue_location" class="form-control" multiple="multiple">
                                    <option value="dhaka">DHAKA</option>
                                    <option value="chttagong">CHITTAGONG</option>
                                    <option value="sylhet">SYLHET</option>
                                    <option value="rajshahi">RAJSHAHI</option>
                                    <option value="barishal">BARISHAL</option>
                                    <option value="khulna">KHULNA</option>
                                </select>
                            </div>
                        </div><br>
                        <button type="submit" class="btn btn-block">REGISTER
                            <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                    <p>Need <a href="#">help?</a></p>
                </div>
            </div>
        </div>
    </div>

</div>

    <!-- Container (Contact Section) -->
<div id="contact" class="container">
    <h3 class="text-center">Contact</h3>
    <p class="text-center"><em>We love our fans!</em></p>

    <div class="row">
        <div class="col-md-4">
            <p>Fan? Drop a note.</p>
            <p><span class="glyphicon glyphicon-map-marker"></span>Chicago, US</p>
            <p><span class="glyphicon glyphicon-phone"></span>Phone: +00 1515151515</p>
            <p><span class="glyphicon glyphicon-envelope"></span>Email: mail@mail.com</p>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                </div>
            </div>
            <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
            <br>
            <div class="row">
                <div class="col-md-12 form-group">
                    <button class="btn pull-right" type="submit">Send</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <h3 class="text-center">From The Blog</h3>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Nowshin</a></li>
        <li><a data-toggle="tab" href="#menu1">Suraiya</a></li>
        <li><a data-toggle="tab" href="#menu4">Sadia</a></li>
        <li><a data-toggle="tab" href="#menu2">Trisha</a></li>
        <li><a data-toggle="tab" href="#menu3">Kanij</a></li>
        <li><a data-toggle="tab" href="#menu5">Bithi</a></li>
        <li><a data-toggle="tab" href="#menu6">Tasnin</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h2>Nowshin,Team Leader</h2>
            <p>Man, we've been on the road for some time now. Looking forward to lorem ipsum.</p>
        </div>
        <div id="menu1" class="tab-pane fade">
            <h2>Suraiya,Member</h2>
            <p>Always a pleasure people! Hope you enjoyed it as much as I did. Could I BE.. any more pleased?</p>
        </div>
        <div id="menu4" class="tab-pane fade">
            <h2>Sadia,Member</h2>
            <p>Always a pleasure people! Hope you enjoyed it as much as I did. Could I BE.. any more pleased?</p>
        </div>
        <div id="menu2" class="tab-pane fade">
            <h2>Trisha, Team Member</h2>
            <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
        </div>
        <div id="menu3" class="tab-pane fade">
            <h2>Kanij, Team Member</h2>
            <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
        </div>
        <div id="menu5" class="tab-pane fade">
            <h2>Bithi, Team Member</h2>
            <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
        </div>
        <div id="menu6" class="tab-pane fade">
            <h2>Tasnin,team Member</h2>
            <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
        </div>

    </div>
</div>

<!-- Add Google Maps -->
<div id="googleMap"></div>
<script>
    function myMap() {
        var myCenter = new google.maps.LatLng(41.878114, -87.629798);
        var mapProp = {center:myCenter, zoom:12, scrollwheel:false, draggable:false, mapTypeId:google.maps.MapTypeId.ROADMAP};
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        var marker = new google.maps.Marker({position:myCenter});
        marker.setMap(map);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->

<!-- Footer -->
<footer class="text-center">
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Theme Made By <a href="https://.com" data-toggle="tooltip" title="Visit skutz">Skutz</a></p>
</footer>


<script src="../../../resource/bootstrap/js/jquery.js"></script>


<script>

    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })



    $(document).ready(function(){
        // Initialize Tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {

                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    })
</script>

</body>
</html>
