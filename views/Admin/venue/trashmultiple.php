<?php
require_once("../../../vendor/autoload.php");

use \App\Admin\venue\Location;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$objVenue= new Location();


    $objVenue->trashMultiple($_POST['mark']);
    Utility::redirect("trashed.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("index.php");
}